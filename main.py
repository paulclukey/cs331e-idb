from flask import Flask, render_template, request, jsonify, make_response
import sqlalchemy
import requests
import pandas as pd
import sys
from create_db import app, db, Club, Player, Fixture

pd.set_option('max_colwidth', None)
pd.set_option('display.max_rows', None)

gl_token = 'glpat-tiZYEUhgzrz9f8qXAiFC'

# Format is name (string), bio (string), image path (string), responsibilties (list of strings), gitlab id (string), number of unit tests (int), gitlab email (string)
# Unit tests hard coded for now, could not find API for this
maintainersInfo = [
    ["Michael Sams","Michael Sams is a fourth year Mechanical Engineering student with an Elements of Computing Certificate, graduating in May 2023. He has had internships in game development and in data automation. After college he plans to do a software engineering bootcamp and continue his programming pursuit.", "/static/images/Michaelsams.jpg", ["Home, About Us pages, & Wiki", "Unit Testing"], "12426685", 9, "michaelsams247@gmail.com"],
    ["Paul Clukey","Paul Clukey is a fourth year Mechanical Engineering student with an Elements of Computing Certificate, graduating in May 2023. He has been a member of the engineering design team Longhorn Racing Electric, an electric Formula SAE team at The University of Texas at Austin, for the past four years on both the Dynamics and Powertrain sub teams. He is interested in going into the Software Engineering field after graduation.", "/static/images/paulc.jpg", ["Programmatically populate repository stats using the GitLab REST API.", "Players and Games pages.", "Flask backend.", "Table pagination.", "GCP setup and deployment (App Engine and PostgreSQL Database).", "CI/CD Pipeline and integration into webpage.", "Internal API implementation.", "Internal API testing using Postman.", "About Us page.", "Wiki.", "Presentation."], "10652723", 44, "paulclukey@utexas.edu"],
    ["Ralph(Yu-Huan), Chang", "Ralph(Yu-Huan), Chang is a third year Sociology student with an Elements of Computing Certificate. He is currently an UX designer for Texas Convergent Non-Profit Forge team, a business and computer science organization at The University of Texas at Austin. He is interested in going into UI/UX design field after graduation.", "/static/images/Ralph.jpg", ["Club page.", "Pull data from external APIs to populate database."], "12355367", 0, "ralphchang@utexas.edu"],
    ["Evan Plunkett", "Evan Plunkett is a senior Mathematics student with an Elements of Computing Certificate at The University of Texas. He is currently a web developer for a local sports business.", "/static/images/evan.jpeg", ["Idea formulation, makefile, IDB1.log file, and previous NBA technical fouls page"], "10686990", 0, "evanmplunkett@gmail.com"]
    ]

with app.app_context():
    # players table has player_id (int), player_name (string), age (int), total_games, goals (int), yellow_cards (int), red_cards(int), link (str), player_picture (str), club_id (int)
    query = db.session.query(Player).all()
    player_data = pd.DataFrame([vars(item) for item in query])

    # clubs table has club_id (int), club_name (string), club_short_name (str), country (str), league (str), founded (int), club_link (str), club_picture (str)
    query = db.session.query(Club).all()
    club_data = pd.DataFrame([vars(item) for item in query])

    # fixture table has fixture_id (int), kickoff_time (date), team_a_id (int), team_h_id (int), team_a_score (int), team_h_score (int)
    query = db.session.query(Fixture).all()
    fixture_data = pd.DataFrame([vars(item) for item in query])

"""
join tables
"""
# merge players and clubs table on club_id column
merged_data = pd.merge(player_data, club_data, on='club_id')

# merge fixture table with merged_data on team_a_id and team_h_id columns
final_data = pd.merge(merged_data, fixture_data, left_on='club_id', right_on='team_a_id')
final_data = final_data.merge(club_data, left_on='team_h_id', right_on='club_id', suffixes=('_team_a', '_team_h'))

# Game Data
game_data = pd.merge(fixture_data, club_data, left_on="team_a_id", right_on="club_id", suffixes=('', '_team_a'))
game_data = pd.merge(game_data, club_data, left_on="team_h_id", right_on="club_id", suffixes=('', '_team_h'))

@app.route('/')
def index():
    data = merged_data.sort_values(['red_cards', 'yellow_cards'], ascending=(False, False)).head(5)
    return render_template('index.html', data=data)

@app.route('/players', methods=['GET', 'POST'])
def players():
    if request.method == 'POST':
        search_string = request.form['search-string']
        search_by = request.form['search-by']
        sort_by = request.form['sort-by']
        sort_by_option = request.form['sort-by-option']
        data = merged_data
        mask = ""
        if not search_by:
            mask = data[
                ["player_name", "club_name", "league", "total_games", "goals", "assists", "yellow_cards",
                 "red_cards"]].apply(
                lambda x: x.astype("string").str.contains(
                    search_string,
                    regex=True,
                    case=False
                )
            ).any(axis=1)
        else:
            mask = data[[search_by]].apply(
                lambda x: x.astype("string").str.contains(
                    search_string,
                    regex=True,
                    case=False
                )
            ).any(axis=1)
        if sort_by:
            data = data.sort_values(sort_by, ascending=(sort_by_option == "asc"))
        return render_template('players.html', data=data[mask], search_string=search_string, search_by=search_by, sort_by=sort_by, sort_by_option=sort_by_option)
    else:
        data = merged_data
        return render_template('players.html', data=data)

@app.route('/players/<int:player_id>')
def player(player_id):
    data = merged_data[merged_data["player_id"]==player_id].T.squeeze()
    return render_template('player.html', data=data, game_data=game_data)

@app.route('/games', methods=['GET', 'POST'])
def games():
    if request.method == 'POST':
        search_string = request.form['search-string']
        search_by = request.form['search-by']
        sort_by = request.form['sort-by']
        sort_by_option = request.form['sort-by-option']
        data = game_data.fillna(0)
        mask = ""
        if not search_by:
            mask = data[["kickoff_time", "club_name_team_h", "team_h_score", "team_h_penalty", "club_name", "team_a_score", "team_a_penalty"]].apply(
                lambda x: x.astype("string").str.contains(
                    search_string,
                    regex=True,
                    case=False
                )
            ).any(axis=1)
        else:
            mask = data[[search_by]].apply(
                lambda x: x.astype("string").str.contains(
                    search_string,
                    regex=True,
                    case=False
                )
            ).any(axis=1)
        if sort_by:
            data = data.sort_values(sort_by, ascending=(sort_by_option == "asc"))
        return render_template('games.html', data=data[mask], search_string=search_string, search_by=search_by, sort_by=sort_by, sort_by_option=sort_by_option)
    else:
        data = game_data.fillna(0)
        return render_template('games.html', data=data)

@app.route('/games/<int:game_id>')
def game(game_id):
    data = game_data[game_data["fixture_id"] == game_id].T.squeeze()
    players_a = player_data.loc[player_data['club_id'] == data['team_a_id']]
    players_h = player_data.loc[player_data['club_id'] == data['team_h_id']]
    return render_template('game.html', data=data, players_a = players_a, players_h = players_h)

@app.route('/clubs', methods=['GET', 'POST'])
def clubs():
    if request.method == 'POST':
        search_string = request.form['search-string']
        search_by = request.form['search-by']
        sort_by = request.form['sort-by']
        sort_by_option = request.form['sort-by-option']
        data = club_data
        mask = ""
        if not search_by:
            mask = data[["club_name", "league", "club_short_name", "founded"]].apply(
                lambda x: x.astype("string").str.contains(
                    search_string,
                    regex=True,
                    case=False
                )
            ).any(axis=1)
        else:
            mask = data[[search_by]].apply(
                lambda x: x.astype("string").str.contains(
                    search_string,
                    regex=True,
                    case=False
                )
            ).any(axis=1)
        if sort_by:
            data = data.sort_values(sort_by, ascending=(sort_by_option == "asc"))
        return render_template('clubs.html', data=data[mask])
    else:
        data = club_data
        return render_template('clubs.html', data=data)

@app.route('/clubs/<int:club_id>')
def club(club_id):
    club_d = club_data.loc[club_data['club_id'] == club_id]
    club_name = club_d['club_name'].to_string(index=False, header=False)
    club_picture = club_d['club_picture'].to_string(index=False, header=False)
    players_d = player_data.loc[player_data['club_id'] == club_id]
    goals = players_d['goals'].sum()
    assists = players_d['assists'].sum()
    yellow_cards = players_d['yellow_cards'].sum()
    red_cards = players_d['red_cards'].sum()
    return render_template('club.html', name = club_name, picture = club_picture, players = players_d,
                           goals = goals, assists=assists, yellow_cards=yellow_cards, red_cards=red_cards,
                           club_id=club_id, game_data=game_data)

@app.route('/aboutus')
def aboutus():
    length = 1
    page = 0
    num_commits = "Error Processing GitLab API"
    while length > 0:
        page += 1
        length = 0
        # API request for commit history
        response = requests.get("https://gitlab.com/api/v4/projects/43026984/repository/commits?per_page=100&page=" + str(page), headers={'PRIVATE-TOKEN': gl_token, 'Content-Type': 'application/json'})
        response_json = response.json()
        if response.status_code == 200:
            if type(num_commits) is str:
                num_commits = len(response_json)
            elif type(num_commits) is int:
                num_commits += len(response_json)
            length = len(response_json)

    length = 1
    page = 0
    num_issues = "Error Processing GitLab API"
    while length > 0:
        page += 1
        length = 0
        # API request for repo issues
        response = requests.get("https://gitlab.com/api/v4/projects/43026984/issues?scope=all&per_page=100&page=" + str(page), headers={'PRIVATE-TOKEN': gl_token, 'Content-Type': 'application/json'})
        response_json = response.json()
        if response.status_code == 200:
            if type(num_issues) is str:
                num_issues = len(response_json)
            elif type(num_issues) is int:
                num_issues += len(response_json)
            length = len(response_json)

    return render_template('aboutus.html', len = len(maintainersInfo), info = maintainersInfo, num_commits=num_commits, num_issues=num_issues)

@app.route('/aboutus/<int:idx>')
def maintainer(idx):
    length = 1
    page = 0
    num_commits = "Error Processing GitLab API"
    while length > 0:
        page += 1
        length = 0
        commit_count = 0
        # API request for commit history
        response = requests.get("https://gitlab.com/api/v4/projects/43026984/repository/commits?per_page=100&page=" + str(page), headers={'PRIVATE-TOKEN': gl_token, 'Content-Type': 'application/json'})
        response_json = response.json()
        if response.status_code == 200:
            for commit in response_json:
                if commit['committer_email'] == maintainersInfo[idx][6]:
                    commit_count += 1
            if type(num_commits) is str:
                num_commits = commit_count
            elif type(num_commits) is int:
                num_commits += commit_count
            length = len(response_json)

    length = 1
    page = 0
    num_issues = "Error Processing GitLab API"
    while length > 0:
        page += 1
        length = 0
        # API request for repo issues
        response = requests.get("https://gitlab.com/api/v4/projects/43026984/issues?scope=all&per_page=100&page=" + str(page) + "&author_id=" + maintainersInfo[idx][4], headers={'PRIVATE-TOKEN': gl_token, 'Content-Type': 'application/json'})
        response_json = response.json()
        if response.status_code == 200:
            if type(num_issues) is str:
                num_issues = len(response_json)
            elif type(num_issues) is int:
                num_issues += len(response_json)
            length = len(response_json)

    return render_template("maintainer.html", currentIdx = idx, len=len(maintainersInfo), info = maintainersInfo, num_commits=num_commits, num_issues=num_issues)

@app.route('/api/players', methods=['GET'])
def get_players():
    data = ''
    if request.args.get('id') is None and request.args.get('club_id') is None:
        data = db.session.query(Player).all()
    elif request.args.get('club_id') is None:
        data = db.session.query(Player).filter(Player.player_id == request.args.get('id')).all()
    elif request.args.get('id') is None:
        data = db.session.query(Player).filter(Player.club_id == request.args.get('club_id')).all()
    else:
        data = db.session.query(Player).filter(Player.player_id == request.args.get('id'), Player.club_id == request.args.get('club_id')).all()

    response = make_response(jsonify(data))
    if len(data) == 0:
        # Successful search but no element with that id found
        response.status = '404 Player(s) Not Found'
    else:
        response.status_code = 200
    return response

@app.route('/api/clubs', methods=['GET'])
def get_clubs():
    data = db.session.query(Club).all() if request.args.get('id') is None else db.session.query(Club).filter(Club.club_id == request.args.get('id')).all()
    response = make_response(jsonify(data))
    if len(data) == 0:
        # Successful search but no element with that id found
        response.status = '404 Club(s) Not Found'
    else:
        response.status_code = 200
    return response

@app.route('/api/games', methods=['GET'])
def get_games():
    data = ''
    if request.args.get('id') is None and request.args.get('club_id') is None:
        data = db.session.query(Fixture).all()
    elif request.args.get('club_id') is None:
        data = db.session.query(Fixture).filter(Fixture.fixture_id == request.args.get('id')).all()
    elif request.args.get('id') is None:
        data = db.session.query(Fixture).filter((Fixture.team_a_id == request.args.get('club_id')) | (Fixture.team_h_id == request.args.get('club_id'))).all()
    else:
        data = db.session.query(Fixture).filter((Fixture.fixture_id == request.args.get('id')) & ((Fixture.team_a_id == request.args.get('club_id')) | (Fixture.team_h_id == request.args.get('club_id')))).all()

    response = make_response(jsonify(data))
    if len(data) == 0:
        # Successful search but no element with that id found
        response.status = '404 Game(s) Not Found'
    else:
        response.status_code = 200
    return response

if __name__ == "__main__":
    app.run(debug=True)
    #app.run(host='0.0.0.0')
