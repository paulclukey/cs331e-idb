ifeq ($(shell uname), Darwin)          # Apple
    PYDOC    := pydoc3
    COVERAGE := coverage
else ifeq ($(shell uname -p), unknown) # Windows
    PYDOC    := python -m pydoc
    COVERAGE := coverage
else                                   # UTCS
    PYDOC    := pydoc3
    COVERAGE := coverage
endif

models.html: models.py
	$(PYDOC) -w models

TestModels.tmp: test.py
	$(COVERAGE) run 	--branch test.py > TestModels.tmp 2>&1
	$(COVERAGE) report  -m			   >> TestModels.tmp
	cat TestModels.tmp

log:
	git log > IDB3.log

clean:
	-rm IDB3.log
	rm -f .coverage
	rm -f TestModels.tmp

