import os
import sys
import unittest
from models import db, Player, Club, Fixture

db.create_all()
class DBTestCases(unittest.TestCase):
	
	def test_player_1(self):
		#Test for adding new player to database

		player_test = Player(player_id=100000, player_name="Nicolas Cage", age=59, goals=5, nationality= "NA", total_games=4, position="forward", assists=2,yellow_cards=10, red_cards=20, player_picture="https://www.image.com")

		db.session.add(player_test)
		db.session.commit()

		query_player = db.session.query(Player).filter_by(player_id = 100000).one()
		self.assertEqual(query_player.player_name, 'Nicolas Cage')
		self.assertEqual(query_player.age, 59)
		self.assertEqual(query_player.goals, 5)
		self.assertEqual(query_player.yellow_cards,10)
		self.assertEqual(query_player.red_cards, 20)
		self.assertEqual(query_player.player_picture, 'https://www.image.com')
		
	def test_player_2(self):
		# Test for changing player attribute 

		query_player = db.session.query(Player).filter_by(player_id = 100000).one()
		query_player.goals = 10
		query_player.yellow_cards = 15
		query_player.red_cards = 25
		db.session.commit()

		updated_query_player = db.session.query(Player).filter_by(player_id = 100000).one()
		self.assertEqual(updated_query_player.goals, 10)
		self.assertEqual(updated_query_player.yellow_cards, 15)
		self.assertEqual(updated_query_player.red_cards, 25)

	def test_player_3(self):
		# Test for deleting a player

		query_player = db.session.query(Player).filter_by(player_id = 100000).one()
		db.session.delete(query_player)
		db.session.commit()

		updated_query_player = db.session.get(Player, 100000)
		self.assertIsNone(updated_query_player)
	
	def test_club_1(self):
		# Test for adding new club to database

		club_test = Club(club_id=100001, club_name="My New Team", club_short_name= "New", country="US", league="Temp", founded = 2023, club_picture="https://www.myNewTeam.com/image")

		db.session.add(club_test)
		db.session.commit()

		query_club = db.session.query(Club).filter_by(club_id = 100001).one()
		
		self.assertEqual(query_club.club_name, "My New Team")
		self.assertEqual(query_club.club_short_name, "New")
		self.assertEqual(query_club.country, "US")
		self.assertEqual(query_club.league, "Temp")
		self.assertEqual(query_club.founded, 2023)
		self.assertEqual(query_club.club_picture, "https://www.myNewTeam.com/image")

	def test_club_2(self):
		# Test for updating attributes

		query_club = db.session.query(Club).filter_by(club_id= 100001).one()
		query_club.club_name = "My Old Team"
		query_club.founded = 2021
		db.session.commit()

		updated_query_club = db.session.query(Club).filter_by(club_id = 100001).one()
		self.assertEqual(updated_query_club.club_name, "My Old Team")
		self.assertEqual(updated_query_club.founded, 2021)

	def test_club_3(self):
		# Test for deleting club

		query_club = db.session.query(Club).filter_by(club_id = 100001).one()
		db.session.delete(query_club)
		db.session.commit()

		updated_query_club = db.session.get(Club, 100001)
		self.assertIsNone(updated_query_club)

	def test_fixture_1(self):
		# Test for adding a new league

		league_test = Fixture(fixture_id=100002, kickoff_time="2023-02-02", team_a_id=40, team_h_id=50, team_a_score = 1, team_h_score=2, team_a_penalty=1, team_h_penalty=3)

		db.session.add(league_test)
		db.session.commit()

		query_league = db.session.query(Fixture).filter_by(fixture_id = 100002).one()
		self.assertEqual(query_league.team_a_id, 40 )
		self.assertEqual(query_league.team_h_id, 50 )
		self.assertEqual(query_league.team_a_score, 1)
		self.assertEqual(query_league.team_h_score, 2)
		self.assertEqual(query_league.team_a_penalty, 1)
		self.assertEqual(query_league.team_h_penalty, 3)

	def test_fixture_2(self):
		# Test for updating League attributes

		query_league = db.session.query(Fixture).filter_by(fixture_id = 100002).one()
		query_league.team_a_score = 4
		query_league.team_h_score = 2

		db.session.commit()

		updated_query_league = db.session.query(Fixture).filter_by(fixture_id = 100002).one()
		self.assertEqual(updated_query_league.team_a_score, 4)
		self.assertEqual(updated_query_league.team_h_score, 2)
	
	def test_fixture_3(self):
		# Test for deleting a League

		query_league = db.session.query(Fixture).filter_by(fixture_id = 100002).one()
		db.session.delete(query_league)
		db.session.commit()

		updated_query_league = db.session.get(Fixture, 100002)
		self.assertIsNone(updated_query_league)

if __name__ == '__main__': # pragma: no cover
    unittest.main()