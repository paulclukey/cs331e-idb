from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
from dataclasses import dataclass
import datetime

DBUSER = "postgres"
DBPORT = "5432"
DBPASS = "3md&8GFq"
#DBPASS = "Password247"
DBNAME = "soccerdb"

app = Flask(__name__)

# Host name must be postgres when running CI/CD, localhost when running locally
HOST = os.environ.get('HOST', 'localhost')
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DB_STRING', 'postgresql://' + DBUSER + ':' + DBPASS + '@' + HOST + ':' + DBPORT + '/' + DBNAME)
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://ralph:Rain0210@localhost:5433/soccerdb'

# to suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.app_context().push()
db = SQLAlchemy(app)

link1 = db.Table('link1',
   db.Column('player_id', db.Integer, db.ForeignKey('player.player_id')),
   db.Column('fixture_id', db.Integer, db.ForeignKey('fixture.fixture_id'))
   )

link2 = db.Table('link2',
   db.Column('club_id', db.Integer, db.ForeignKey('club.club_id')),
   db.Column('fixture_id', db.Integer, db.ForeignKey('fixture.fixture_id'))
   )

@dataclass
class Player(db.Model):
    __tablename__ = 'player'

    # Specify datatypes of columns
    player_id: int
    player_name: str
    age: int
    goals: int
    nationality: str
    total_games: int
    position: str
    assists: int
    yellow_cards: int
    red_cards: int
    player_picture: str
    club_id: int

    player_id = db.Column(db.Integer, primary_key=True)
    player_name = db.Column(db.String(80), nullable=False)
    age = db.Column(db.Integer, nullable=True)
    goals = db.Column(db.Integer, nullable=False)
    nationality = db.Column(db.String(80), nullable=True)
    total_games = db.Column(db.Integer, nullable=False)
    position = db.Column(db.String(80), nullable=True)
    assists = db.Column(db.Integer, nullable = False)
    yellow_cards = db.Column(db.Integer, nullable=False)
    red_cards = db.Column(db.Integer, nullable=False)
    player_picture = db.Column(db.String(80), nullable=False)

    # Foreign Key to Team
    club_id = db.Column(db.Integer, db.ForeignKey('club.club_id'))

@dataclass
class Club(db.Model):
    __tablename__ = 'club'

    # Specify datatypes of columns
    club_id: int
    club_name: str
    club_short_name: str
    country: str
    league: str
    founded: int
    club_picture: str

    club_id = db.Column(db.Integer, primary_key=True)
    club_name = db.Column(db.String(80), nullable=False)
    club_short_name = db.Column(db.String(80), nullable=False)
    country = db.Column(db.String(80), nullable=False)
    league = db.Column(db.String(80), nullable=False)
    founded = db.Column(db.Integer, nullable=False)
    club_picture = db.Column(db.String(80), nullable=False)

    # Virtual Column reference to players
    players = db.relationship('Player', backref='club')

@dataclass
class Fixture(db.Model):
    __tablename__ = 'fixture'

    fixture_id: int
    kickoff_time: datetime.datetime
    team_a_id: int
    team_h_id: int
    team_a_score: int
    team_h_score: int
    team_a_penalty: int
    team_h_penalty: int

    fixture_id = db.Column(db.Integer, primary_key = True)
    kickoff_time = db.Column(db.Date, nullable = False)
    team_a_id = db.Column(db.Integer, nullable = False)
    team_h_id = db.Column(db.Integer, nullable = False)
    team_a_score = db.Column(db.Integer, nullable = False)
    team_h_score = db.Column(db.Integer, nullable = False)
    team_a_penalty = db.Column(db.Integer, nullable = False)
    team_h_penalty = db.Column(db.Integer, nullable = False)

    # Virtual Column reference to players
    players = db.relationship('Player', secondary = 'link1', backref='play')
    clubs = db.relationship('Club', secondary='link2', backref='join')

# If drop all commented and tables already exist, create all will do nothing, even if column definitions are changed
# It will add new tables however
with app.app_context():
    # db.drop_all()
    db.create_all()
