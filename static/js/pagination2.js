<script>
  const CARDS_PER_PAGE = 6;

  const cardContainer = document.getElementById('card-container');
  const cards = cardContainer.querySelectorAll('.card');
  const numPages = Math.ceil(cards.length / CARDS_PER_PAGE);
  showPage(1);

  const pagination = document.querySelector('.pagination');
  pagination.addEventListener('click', (event) => {
    event.preventDefault();
    const button = event.target.closest('.page-link');
    if (!button) return;
    if (button.classList.contains('disabled')) return; // don't do anything if button is disabled
    const page = parseInt(button.textContent);
    if (isNaN(page)) return;
    showPage(page);
    updateActivePage(page);
    updatePrevNextButtons(page);
  });

  function showPage(page) {
    const startIndex = (page - 1) * CARDS_PER_PAGE;
    const endIndex = startIndex + CARDS_PER_PAGE;
    cards.forEach((card, index) => {
      if (index >= startIndex && index < endIndex) {
        card.style.display = '';
      } else {
        card.style.display = 'none';
      }
    });
  }

  function updateActivePage(page) {
    const activeItem = pagination.querySelector('.active');
    if (activeItem) {
      activeItem.classList.remove('active');
    }
    const pageItems = pagination.querySelectorAll('.page-item');
    const currentItem = pageItems[page];
    currentItem.classList.add('active');
  }

  function updatePrevNextButtons(currentPage) {
    const prevButton = pagination.querySelector('.prev-link');
    const nextButton = pagination.querySelector('.next-link');
    if (currentPage === 1) {
      prevButton.classList.add('disabled');
    } else {
      prevButton.classList.remove('disabled');
    }
    if (currentPage === numPages) {
      nextButton.classList.add('disabled');
    } else {
      nextButton.classList.remove('disabled');
    }
  }
</script>