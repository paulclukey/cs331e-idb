const pipelineIDDiv = document.getElementById("pipeline-id");
const pipelineStatusDiv = document.getElementById("pipeline-status");
const runTestsButton = document.getElementById("run-tests");

function getStatus(){
    // Create XMLHttpRequest object
    const xhr = new XMLHttpRequest()
    // Open request
    xhr.open("GET", "https://gitlab.com/api/v4/projects/43026984/pipelines?per_page=1&page=1")
    // Set headers
    xhr.setRequestHeader('PRIVATE-TOKEN', 'glpat-tiZYEUhgzrz9f8qXAiFC')
    xhr.setRequestHeader('Content-Type', 'application/json')
    // Send the Http request
    xhr.send()

    // Triggered when the response is completed
    xhr.onload = function() {
        if (xhr.status === 200) {
            // Parse JSON data
            data = JSON.parse(xhr.responseText)
            pipelineIDDiv.innerHTML = "Current Pipeline ID: " + String(data[0]['id'])
            pipelineStatusDiv.innerHTML = "Pipeline Status: " + String(data[0]['status']).charAt(0).toUpperCase() + String(data[0]['status']).slice(1)

            // If pipeline response one of created, waiting_for_resource, preparing, pending, or running then check again in 10 seconds
            // Disable run tests button in this time
            if (['created', 'waiting_for_resource', 'preparing', 'pending', 'running'].includes(String(data[0]['status']))) {
                runTestsButton.setAttribute("disabled", "")
                setTimeout(getStatus, 10000)
            } else {
                runTestsButton.removeAttribute("disabled");
            }
        } else {
            pipelineIDDiv.innerHTML = "Current Pipeline ID: Error Processing GitLab API"
            pipelineStatusDiv.innerHTML = "Pipeline Status: Error Processing GitLab API"
        }
    }
}

function runTests(){
    // Create XMLHttpRequest object
    const xhr = new XMLHttpRequest()
    // Open request
    xhr.open("POST", "https://gitlab.com/api/v4/projects/43026984/trigger/pipeline?token=glptt-46724aa23f8882c82f52eb8e66f35b9ce5ef70b9&ref=main")
    // Send the Http request
    xhr.send()

    // Triggered when the response is completed
    xhr.onload = function() {
        if (xhr.status === 201) {
            // Call getStatus()
            getStatus()
        } else {
            console.log("Error sending POST request to run unit tests")
        }
    }
}
