const paginationNav = document.getElementById("page-nav");
const dataTable = document.getElementById("data-table");
const paginationLimit = 10;

function createButtons(numItems, curPage){
    let pageCount = Math.ceil(numItems / paginationLimit);
    let currentPage = curPage;

    // Create Previous Button
    makeButton("Previous", curPage-1, numItems, false, (curPage==1));

    if (curPage > 1) {
        // Create First Page Button
        makeButton("1", 1, numItems);
    }

    if (curPage > 4) {
        // Create ... Button
        makeButton("...", 0, numItems, false, true);
    } else if (curPage == 4) {
        // Create Second Page Button
        makeButton("2", 2, numItems);
    }

    if (curPage > 2) {
        // Create Page Button for curPage - 1
        makeButton((curPage - 1).toString(), curPage-1, numItems);
    }

    // Create Page Button for curPage
    makeButton(curPage.toString(), curPage, numItems, true);

    if (curPage + 1 < pageCount) {
        // Create Page Button for curPage + 1
        makeButton((curPage + 1).toString(), curPage+1, numItems);
    }

    if (curPage + 3 < pageCount) {
        // Create ... Page
        makeButton("...", 0, numItems, false, true);
    } else if (curPage + 3 == pageCount) {
        // Create Page for pageCount - 1
        makeButton((pageCount - 1).toString(), pageCount-1, numItems);
    }

    if (curPage < pageCount) {
        // Create Last Page Button
        makeButton(pageCount.toString(), pageCount, numItems);
    }

    // Create Next Button
    makeButton("Next", curPage+1, numItems, false, (curPage==pageCount));

    // Show correct list items
    updateTable(numItems, curPage);
}

function makeButton(s, newPageNum, numItems, cur=false, dis=false) {
    let pageButtonDiv = document.createElement("div");
    pageButtonDiv.classList.add('btn-group', 'w-100');
    pageButtonDiv.setAttribute("role", "group");
    let pageButton = document.createElement("button");
    pageButton.classList.add('btn', 'btn-primary');
    if (cur) {
        pageButton.classList.add('active');
    }
    pageButton.setAttribute("type", "button");
    if (dis) {
        pageButton.setAttribute("disabled", "");
    }
    pageButton.innerHTML = s;
    pageButton.addEventListener("click", () => {
        paginationNav.childNodes[1].innerHTML = '';
        createButtons(numItems, newPageNum)
      });
    pageButtonDiv.appendChild(pageButton);
    paginationNav.childNodes[1].appendChild(pageButtonDiv);
}

function updateTable(numItems, curPage) {
    // Hide list items in table not on current page
    let startIndex = (curPage-1)*paginationLimit;
    let endIndex = curPage*paginationLimit;
    for (let i = 0; i < numItems; i++) {
        let ele = dataTable.childNodes[3].childNodes[i*2 + 1]
        console.log(ele)
        if (i >= startIndex && i < endIndex) {
            ele.style.display = '';
        } else {
            ele.style.display = 'none';
        }
    }
}
