import json
from models import app, db, Club, Player, Fixture, link1, link2
import requests
import time
from sqlalchemy.exc import IntegrityError

def call_api(endpoint, params={}):
    parameters = ''
    if len(params) > 0:
        parameters = '?' + '&'.join([f'{k}={v}' for k, v in params.items()])

    headers = {
        'x-rapidapi-key': '6b0e41e0fef9ae35a05bb098f3a2da57',
    }

    response = requests.get(f'https://v3.football.api-sports.io/{endpoint}{parameters}', headers=headers)
    return response.json()

def players_data(league, season, page=1, data=[]):
    players = call_api('players', {'league': league, 'season': season, 'page': page})
    data += players['response']

    if players['paging']['current'] < players['paging']['total']:
        page = players['paging']['current'] + 1
        if page % 2 == 1:
            time.sleep(1)
        data = players_data(league, season, page, data)

    return data

# Get all the teams from this competition
#teams = call_api('teams', {'league': 39, 'season': 2022})
file1 = open('dbdata/clubs.json', 'r')
clubs = json.loads(file1.read()) #teams['response']
file1.close()

# Get all the players from this competition
file2 = open('dbdata/players.json', 'r')
players = json.loads(file2.read()) #players_data(39, 2022)
file2.close()

#f_data = call_api('/fixtures', {'league': 39, 'season': 2022})
file3 = open('dbdata/fixtures.json', 'r')
fixtures = json.loads(file3.read()) #f_data['response']
file3.close()

def create_clubs():
    # Populate Club table
    with app.app_context():
        for club in clubs:
            club_name = club['team']['name']
            club_id = club['team']['id']
            club_short_name = club['team']['code']
            club_founded = club['team']['founded']
            club_picture = club['team']['logo']
            club_league = 'Premier League'
            club_country = "England"
            newclub = Club(club_name=club_name, club_id=club_id, club_short_name = club_short_name, founded = club_founded,
                           country = club_country, league= club_league, club_picture = club_picture)

            db.session.merge(newclub)
            db.session.commit()


def create_players():
    # Populate player table
    with app.app_context():
        i = 0
        for player in players:
            k = player['statistics'][i]['team']['id']
            if k < 33 or k > 66:
                continue
            player_fname = player['player']["firstname"]
            player_lname = player['player']["lastname"]
            player_id = player['player']['id']
            player_age = player['player']['age']
            player_nationality = player['player']['nationality']
            player_appearences = player['statistics'][i]['games']['appearences']
            player_position = player['statistics'][i]['games']['position']
            player_assists = player['statistics'][i]['goals']['assists']
            player_goals = player['statistics'][i]['goals']['total']
            player_ycards = player['statistics'][i]['cards']['yellow']
            player_rcards = player['statistics'][i]['cards']['red']
            player_picture = player['player']['photo']

            club_id = player['statistics'][i]['team']['id']

            newplayer = Player(player_name=player_fname + ' ' + player_lname, player_id=player_id,
                               age=player_age, goals=0 if player_goals is None else player_goals,
                               assists=0 if player_assists is None else player_assists,
                               nationality=player_nationality,
                               total_games=0 if player_appearences is None else player_appearences,
                               position=player_position, club_id=club_id,
                               yellow_cards=0 if player_ycards is None else player_ycards,
                               red_cards=0 if player_rcards is None else player_rcards,
                               player_picture=player_picture)

            db.session.merge(newplayer)
            db.session.commit()

def create_fixtures():
    # Populate the fixture (game) table
    with app.app_context():
        for fixture in fixtures:
            fixture_id = fixture['fixture']["id"]
            fixture_kickoff_time = fixture['fixture']["date"]
            team_a_id = fixture["teams"]['away']['id']
            team_h_id = fixture["teams"]['home']['id']
            team_a_score = fixture["goals"]['away']
            team_h_score = fixture["goals"]['home']
            team_a_penalty = fixture['score']['penalty']['away']
            team_h_penalty = fixture['score']['penalty']['home']

            newfixture = Fixture(fixture_id=fixture_id, kickoff_time=fixture_kickoff_time, team_a_id = team_a_id,
                                 team_h_id = team_h_id, 
                                 team_a_score= 0 if team_a_score is None else team_a_score, 
                                 team_h_score = 0 if team_h_score is None else team_h_score,
                                 team_h_penalty = 0 if team_h_penalty is None else team_h_penalty, 
                                 team_a_penalty = 0 if team_a_penalty is None else team_a_penalty)

            db.session.merge(newfixture)
            db.session.commit()


create_clubs()
create_players()
create_fixtures()
